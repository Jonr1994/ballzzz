# This is my README
# Ballzzz is a Unity-based ball platforming game.
# Javascript and C# were used primarily for the programming aspect of the game.
# The free 3D sculpting program, Blender, was used for the creation of 3D objects used in the game.

#The project consists of the following students:
#	Jonathan Reilly (Yours truly)
#	Martin Staunton
#	Sinead Kelly
#	Kevin Nolan
